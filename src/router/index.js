import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from "../views/Home";
import NotFound from "../components/NotFound";
import DevisFruit from "../components/DevisFruit";
import DevisCafe from "../components/DevisCafe";
import DevisEvenementiel from "../components/DevisEvenementiel";
import Produit from "../views/Produit";
import Outils from "../views/Outils";
import Panier from "../components/Panier";
import DetailProduit from "../views/DetailProduit";

Vue.use(VueRouter)

const routes = [
    {
      name: "NotFound",
      path: '*',
      component: NotFound,
    },
    {
        name: "Home",
        path: '/',
        component: Home,
    },
    {
        name:"Produit",
        path: '/Produit',
        component: Produit
    },
    {
        name:"Panier",
        path: '/Panier',
        component: Panier
    },
    {
        name: "Outils",
        path: '/Outils',
        component: Outils
    },
    {
        name: "DetailProduit",
        path: '/Produit/:id',
        component: DetailProduit
    },
    {
        name: "DevisFruit",
        path: '/DevisFruit',
        component: DevisFruit
    },
    {
        name: "DevisEvenementiel",
        path: '/DevisEvenementiel',
        component: DevisEvenementiel
    },
    {
        name: "DevisCafe",
        path: '/DevisCafe',
        component: DevisCafe
    },


]

const router = new VueRouter({
  routes,
  mode: 'history',
  base: process.env.BASE_URL,

});

export default router
